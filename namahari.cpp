#include <iostream>
#include <conio.h>
using namespace std;
int main()
{
	//Input 
	//Baris pertama disi oleh jumlah hari (N) 
	cout << "Masukkan jumlah hari (N) : " ;
	int N;
	cin >> N;
	//Baris kedua disi oleh hari awal
	string hari [] = {"Senin","Selasa","Rabu","Kamis","Jumat","Sabtu","Minggu"};
	cout << "Masukkan hari awal : " ;
	string awal;
	cin >> awal;

	//Output
	//Setelah hari awal sampai hari terakhir sebanyak jumlah hari yang di-input-kan
	for (int i=1; i<=N; i++)
	{
		int j = 0;
		while (hari[j]!=awal)
		{
			j++;
		}
		awal = hari[(j+1)%7];
		cout << "Hari ke " << i+1 << " adalah hari " << awal << endl;
	}
	getch();
	return 0;
}
